# wiicd.sh

wiicd.sh is a simple Wii game cover downloader for the web-less Wii. It detects games on an external source and downloads artwork from http://wiitdb.com to a specified directory. Copy the files on to where the launcher is installed (Wii SD card or Wii external HD).

**NOTE:**
This was useful in 2010 when it was written, most Wii game managers provide this functionality out of the box. Still, in case your Wii is not connected to the Internet and you want to be able to get covers for your games library this script should do the trick.

**REQUIRES:**
WIT: Wiimms ISO Tools https://wit.wiimm.de/

**CONFIG:**
Open the script and edit these values to match your preferences:

- COVER_LANGUAGE=('EN' 'US' 'JA' 'FR' 'PT' 'EN' 'DE')
- BASE_DIRECTORY="./"
- NORMAL_COVER_DIRECTORY=$BASE_DIRECTORY"images/"
- DISK_COVER_DIRECTORY=$NORMAL_COVER_DIRECTORY"disk/"
- COVER_KIND="cover3D" #Options are: cover; coverfull; coverfullHQ; disc; disccustom; cover3D
